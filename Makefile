SHELL=/bin/sh

## Colors
ifndef VERBOSE
.SILENT:
endif
COLOR_COMMENT=\033[0;32m
IMAGE_PATH= /betd/public/docker/phpqa
REGISTRY_DOMAIN=registry.gitlab.com
VERSION=latest
IMAGE=${REGISTRY_DOMAIN}${IMAGE_PATH}:${VERSION}

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## build and push image
all: build push_image

## build image and tags it
build: Dockerfile
	docker build -f Dockerfile . -t ${IMAGE}

## login on registry
registry_login:
	docker login ${REGISTRY_DOMAIN}

## push image
push_image: registry_login
	docker push ${IMAGE}

test:
	docker run --rm -it ${IMAGE} noverify --version
	docker run --rm -it ${IMAGE} phpunit --version
	docker run --rm -it ${IMAGE} phpunit-7 --version
	docker run --rm -it ${IMAGE} phpunit-5 --version
	docker run --rm -it ${IMAGE} yaml-lint --version
	docker run --rm -it ${IMAGE} twig-lint --version
